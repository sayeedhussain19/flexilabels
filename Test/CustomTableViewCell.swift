//
//  CustomTableViewCell.swift
//  Test
//
//  Created by Sayeed Munawar on 10/14/18.
//  Copyright © 2018 Sayeed Munawar. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureWith(title: String, subtitle: String) {
        lbl1.text = title
        lbl2.text = subtitle
    }
    
}
