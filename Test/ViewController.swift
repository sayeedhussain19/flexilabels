//
//  ViewController.swift
//  Test
//
//  Created by Sayeed Munawar on 10/14/18.
//  Copyright © 2018 Sayeed Munawar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let dataSource: [(String, String)] = [
        ("adfasdasdasdasdasdasdasdasdasdasdasdadasdasdasdasdasdasda", "110"),
        ("adfasdasdasdasdasdasdasdasdasdasdasdadasdasdasdasdasdasda", "1"),
        ("dsdsdadasd", "1222"),
        ("Blsss", "23"),
        ("adfasdasdasdasdasdasdasdasdasdasdasdadasdasdasdasdasdasda", "124"),
        ("adfasdasdasdasdasdasdasdasdasdasdasdadasdasdasdasdasdasda", "8888"),
        ("adfasdasdasdasdasdasdasdasdasdasdasdadasdasdasdasdasdasda", "33"),
        ("adfasdasdasdasdasdasdasdasdasdasdasdadasdasdasdasdasdasda", "81"),
        ("sd", "568"),
        ("adfasdasdasdasdasdasdasdasdasdasdasdadasdasdasdasdasdasda", "3"),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {     
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifier") as! CustomTableViewCell
        let data = dataSource[indexPath.row]
        cell.configureWith(title: data.0, subtitle: data.1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

